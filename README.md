### DESCRIÇÃO GERAL
###### Este projeto consiste em reconstruir possíveis trajetórias que animais encalhados puderam ter percorrido baseado no modelo Lagrangeano de transporte de partículas. A partir de um banco de dados construído a partir da base de dados de corrente de livre acesso fornecidos pelo projeto [OSCAR](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjdhO-Q0IftAhXyGLkGHdPvD6IQFjABegQIAxAC&url=https%3A%2F%2Fpodaac.jpl.nasa.gov%2Fdataset%2FOSCAR_L4_OC_1deg&usg=AOvVaw1T9IyMFY6Ir3gOtNqQUwLY), o presente algoritmo implementa o modelo Lagrangeano de forma inversa. A banco de dados atual (16 de novembro de 2020) consiste em dados suficientes para garantir uma execução com êxito na costa brasileira dentro dos limites latitudinais de -10º até o ponto mais ao sul.

### BLIBLIOTECAS NECESSÁRIAS
###### Execute os seguintes comandos no terminal (mínima versão do Python: 3.0).
	* pip install pymongo
	* pip install haversine
	* pip install pandas
	* pip install jupyterlab
	* pip install notebook
	* pip install dnspython
	* pip install gmaps
	* jupyter nbextension enable --py widgetsnbextension
	* jupyter nbextension enable --py gmaps
	
###### OBS.: No caso de linux ou MacOS você terá que exporar o comando jupyter no seu bashprofile: export PATH="$HOME/.local/bin:$PATH"

### EXECUTANDO O MLTP
	* Execute o arquivo main.py contido na pasta *reverse_lagrangian_model*.
	* Preencha os inputs necessários
	* Ao fim da execução você poderá optar entre exporar JSON ou CSV da saída ou visualizar o mapa de calor. Caso opte pelo mapa de calor, você será redirecionado ao visualizador do Jupyter. Acesse a pasta *saídas* e acesse o arquivo *MAPA DE CALOR.ipynb*. Dê RUN.
	
### MAPA DE CALOR
###### Caso queira visualizar o mapa de calor de N execuções prévias, gere arquivos JSON de cada execução. Após, no mesmo terminal, execute o código *jupyter notebook*, vá até o arquivo *MAPA DE CALOR.ipynb* contído dentro da pasta *Saídas* e troque o nome do arquivo JSON para o quisto. Dê RUN.

### ADICIONANDO BASE DE DADOS NO BANCO
###### Gere o arquivo tipo JSON com a base obtida do projeto [OSCAR](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjdhO-Q0IftAhXyGLkGHdPvD6IQFjABegQIAxAC&url=https%3A%2F%2Fpodaac.jpl.nasa.gov%2Fdataset%2FOSCAR_L4_OC_1deg&usg=AOvVaw1T9IyMFY6Ir3gOtNqQUwLY) 1 deg. 
###### Remova as tags desnecessárias, tendo este formato final:
	allData = {
  	"x": ["array de longitudes"]
  	"y": ["array de latitudes"]
  	"dates": ["array de datas"]
  	"uf": [[["velocidades longitudinais"]]]
  	"vf": [[["velocidades latitudinais"]]]
	}
	
###### Converta o arquivo JSON em um arquivo chamado allData.py e insíra-lo na pasta *mongo_populate*. Execute o arquivo *database_populate.py*.
	

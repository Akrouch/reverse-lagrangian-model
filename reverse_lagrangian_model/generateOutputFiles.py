import json
import csv
from time import sleep
import gmaps
import gmaps.datasets
import os

def generateOutputFiles(finalResult):
    stay = True
    while(stay):
        menu = input("\n\n O que deseja fazer?\n1 - Exportar JSON\n2 - Exportar CSV\n3 - Mapa de calor das saídas\n4 - Recomeçar\n5 - Sair\n")
        if menu == "1":
            nome = input('\nNome do arquivo JSON: ')
            with open('saidas/{}.json'.format(nome), 'w') as f:
                json.dump(finalResult, f)
            print('\nArquivo JSON gerado com êxito.\n')
        elif menu == "2":
            nome = input('\nNome do arquivo CSV: ')
            with open('saidas/{}.csv'.format(nome), mode='w') as csv_file:
                strandingLat = finalResult['Coordenada do encalhe'][0]
                strandingLong = finalResult['Coordenada do encalhe'][1]
                strandingDate = finalResult['Data estimada do encalhe']
                returnTime = finalResult['Tempo a ser viajado (dias)']
                latNearestSection = finalResult['Seção mais proxima'][0]
                longNearestSection = finalResult['Seção mais proxima'][1]
                fieldnames = ['Latitude.Encalhe', 'Longitude.Encalhe', 'Data.Encalhe', 'Tempo.Retorno', 'Latitude.Seção.Próxima', 'Longitude.Seção.Próxima', 'Trajetória.Número', 'Latitude.Inicial', 'Longitude.Inicial', 'Distância', 'Tempo', 'Id.Ponto', 'Latitude.Viajado', 'Longitude.Viajado']
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writeheader()
                for x in range(len(finalResult["Trajetorias"])):
                    for z in range(len(finalResult["Trajetorias"][x]["Percurso viajado"])):
                         writer.writerow({'Latitude.Encalhe': strandingLat, 'Longitude.Encalhe': strandingLong, 'Data.Encalhe': strandingDate, 'Tempo.Retorno': returnTime, 'Latitude.Seção.Próxima': latNearestSection, 'Longitude.Seção.Próxima': longNearestSection, 'Trajetória.Número': x, 'Latitude.Inicial': finalResult["Trajetorias"][x]['Coordenada inicial'][0], 'Longitude.Inicial': finalResult["Trajetorias"][x]['Coordenada inicial'][1], 'Distância': finalResult["Trajetorias"][x]['Distancia percorrida (m)'][z], 'Tempo': finalResult["Trajetorias"][x]['Tempo viajado (s)'][z], 'Id.Ponto': z+1, 'Latitude.Viajado': finalResult["Trajetorias"][x]['Percurso viajado'][z][0], 'Longitude.Viajado': finalResult["Trajetorias"][x]['Percurso viajado'][z][1]})
            
            print('\nArquivo CSV gerado com êxito.\n')
        elif menu == "3":
            with open('saidas/heatMap.json', 'w') as f:
                json.dump(finalResult, f)
            os.system('jupyter notebook')
        elif menu == "4":
            stay = False
            return True
        elif menu == "5":
            stay = False
            print("Encerrando...")
            sleep(1)
            return False
            
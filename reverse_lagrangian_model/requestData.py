from pymongo import MongoClient



client = MongoClient('mongodb+srv://scientist:reverse123@cluster0.9beat.mongodb.net/test')

def getAllProcessedData():
    db = client.reverseLagrangianModel
    col = db.sections_with_coast_treatment
    allProcessedData = []
    for doc in col.find():
        allProcessedData.append(doc)
    return allProcessedData


def findObjectByCoordinate(latitude, longitude):
    db = client.reverseLagrangianModel
    col = db.sections_with_coast_treatment
    initial = col.find_one({'latitude': latitude, 'longitude': longitude})
    return initial


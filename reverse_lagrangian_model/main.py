from requestData import getAllProcessedData
from requestData import findObjectByCoordinate
from getNearestSection import getNearestSection
from randomStartingPoints import randomStartingPoints
from getSingleTrajectory import getSingleTrajectory
from generateOutputFiles import generateOutputFiles
import json
from time import sleep
from sys import exit
import random
import csv
from math import trunc

#REQUISITANDO TODOS OS DADOS DO BANCO
print("Requisitando dados do banco. Isso pode levar alguns segundos...")
allProcessedData = getAllProcessedData()
print("Dados requisitados com êxito.\n")

calculateReverseLagrangian = True
while calculateReverseLagrangian:
    #INPUT DE DADOS INICIAIS
    go = False
    while(not go):
        print("Insira a coordenada da carcaça, a data estimada do encalhe e o tempo a ser viajado.")
        inputCoord = [float(input("Latitude: ")), float(input("Longitude: "))]
        inputInitialDate = input("Data estimada do encalhe (YYYYMMDD):")
        inputTime = float(input("Tempo no passado (em dias): "))
        if(inputTime > float(inputInitialDate[6:8])):
            print(inputTime)
            print(inputInitialDate[6:8])
            print("\nO tempo a ser viajado ultrapassa a data do primeiro registro no banco de dados (01 de janeiro de 2020)\n")
        elif(inputInitialDate[4:6] != "01" or inputInitialDate[0:4] != "2020" or len(inputInitialDate) != 8):
            print("\nA base atual não contém dados dessa data.\n")
            sleep(1)
        else:
            go = True
    

    #DETERMINANDO A SEÇÃO MAIS PRÓXIMA
    print("\nDeterminando seção mais próxima para iniciar o MLTP inverso...")
    sleep(2)
    tmp = getNearestSection(inputCoord, allProcessedData)
    nearestDistance = round(tmp[1], 2)
    nearestSection = findObjectByCoordinate(tmp[0][0], tmp[0][1])
    print("Seção inicial e sua distância até a coordenada da carcaça:")
    print("Latitude: {}\nLongitude: {}".format(nearestSection["latitude"], nearestSection["longitude"]))

    #DETERMINANDO PONTOS INICIAIS DENTRO DA SEÇÃO INICIAL
    inputNumberOfStartingPoints = int(input('Insira o número de trajetórias a serem criadas: '))
    inputStandardDeviation = float(input('Insira o desvio padrão a ser utilizado nas direções das correntes: '))
    print("\nDeterminando pontos de partida aleatórios dentro da seção inicial...")
    sleep(1.5)
    initials = randomStartingPoints(nearestSection, inputNumberOfStartingPoints)
    print("{} pontos de partida gerados com êxito.\n".format(len(initials)))


    #Gerando todas as trajetórias
    allTrajectories = []
    for x in range(len(initials)):
        tmp = getSingleTrajectory(nearestSection, initials[x], inputTime, inputInitialDate, x, inputStandardDeviation)
        allTrajectories.append(tmp)

    #Criando dict de saída
    finalResult = {
        "Coordenada do encalhe": inputCoord,
        "Data estimada do encalhe": "{}/{}/{}".format(inputInitialDate[6:8], inputInitialDate[4:6], inputInitialDate[0:4]),
        "Tempo a ser viajado (dias)": inputTime, 
        "Seção mais proxima": [nearestSection["latitude"], nearestSection["longitude"]],
        "Trajetorias": allTrajectories
    }

    #Menu interativo para criação de arquivos
    calculateReverseLagrangian = generateOutputFiles(finalResult)
    
   








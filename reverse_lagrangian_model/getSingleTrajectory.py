from haversine import haversine
from requestData import findObjectByCoordinate
import numpy as np
from math import trunc
from math import sqrt
from math import sin
from random import sample
from math import cos
from math import radians
from sys import exit

def getSingleTrajectory(currentSection, initialCoordenate, timeToTravel, initialDate, x, standardDeviation):
    timeToTravelInSeconds = timeToTravel*24*3600
    traveledTime = 0
    datePos = getInitialDate(initialDate, currentSection)
    traveledDistance = 0
    resultingTrajectory = [initialCoordenate]
    timeVector = [0]
    distanceVector = [0]
    auxTime = 0
    auxDistance = 0
    while(traveledTime < timeToTravelInSeconds):
        timeLeftForThisData = ((float(initialDate)*24*3600 - traveledTime) - (currentSection["dataPerDate"][datePos]["date"] * 24 * 3600) + (24 * 3600))  
        directionsArray = np.random.normal(currentSection["dataPerDate"][datePos]["direction"], standardDeviation, 500)
        directionsList = directionsArray.tolist()
        direction = round(sample(directionsList, 1)[0], 2)
        if direction > 360:
            direction -= 360
        elif direction < 0:
            direction += 360
        tmp = getMaxDistances(direction, resultingTrajectory[-1])
        maxHorizontalDistance = tmp["maxHorizontalDistance"]
        maxVerticalDistance = tmp["maxVerticalDistance"]
        longDegreesPerHorizontalMeter = tmp["longDegreesPerHorizontalMeter"]
        latDegreesPerVerticalMeter = tmp["latDegreesPerVerticalMeter"]
        newCoordinate = resultingTrajectory[-1].copy()
        velocity = currentSection["dataPerDate"][datePos]["resultingVelocity"]
        myVerticalProportion = sqrt((sin(radians(direction)))**2)
        myHorizontalProportion = sqrt((cos(radians(direction)))**2)
        auxTime = 0
        auxDistance = 0
        while(auxTime < timeLeftForThisData and auxDistance*myVerticalProportion < maxVerticalDistance and auxDistance*myHorizontalProportion < maxHorizontalDistance and auxTime < 86400): #and auxTime < 86400
            traveledDistance += velocity
            auxDistance += velocity
            traveledTime += 1
            auxTime += 1
            if(direction >= 0 and direction < 90): #x + and y +
                if(direction != 0):
                    newCoordinate[0] += latDegreesPerVerticalMeter * (velocity * myVerticalProportion)
                newCoordinate[1] += longDegreesPerHorizontalMeter *  (velocity * myHorizontalProportion)
            elif(direction >= 90 and direction < 180): # x - and y +
                newCoordinate[0] += latDegreesPerVerticalMeter * (velocity * myVerticalProportion)
                if(direction != 90):
                   newCoordinate[1] -= longDegreesPerHorizontalMeter * (velocity * myHorizontalProportion)
            elif(direction >= 180 and direction < 270): # x - and y -
                if(direction != 180):
                    newCoordinate[0] -= latDegreesPerVerticalMeter * (velocity * myVerticalProportion)
                newCoordinate[1] -= longDegreesPerHorizontalMeter *  (velocity * myHorizontalProportion)
            elif(direction >= 270 and direction <= 360):
                if(direction != 360):
                    newCoordinate[0] -= latDegreesPerVerticalMeter * (velocity * myVerticalProportion)
                if(direction != 270):
                    newCoordinate[1] += longDegreesPerHorizontalMeter *  (velocity * myHorizontalProportion)
            
        newCoordinate[1] = round(newCoordinate[1], 3)
        newCoordinate[0] = round(newCoordinate[0], 3)
        resultingTrajectory.append(newCoordinate)
        timeVector.append(auxTime)
        distanceVector.append(round(auxDistance, 2))
        #caso necessite de nova data na mesma seção
        if auxTime >= timeLeftForThisData:
            if(datePos > 0):
                datePos -= 1
            else:
                print("#{} Trajetória criada com êxito.".format(x))
                myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                return myJson 
        #caso necessite de uma nova seção em y (latitude)
        if auxDistance*myVerticalProportion >= maxVerticalDistance:
            if(resultingTrajectory[-1][0] > 0.5):
                print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente latitudes acima de 0.5 grau.")
                myJson =  prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                return myJson 
            elif(resultingTrajectory[-1][0] < -60.5):
                print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente latitudes abaixo de -60.5 graus.")
                myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                return myJson 
            else:
                if(direction > 180 and direction < 360):
                    if currentSection["latitude"] > -60.5:
                        currentSection = findObjectByCoordinate((currentSection["latitude"] - 1), currentSection["longitude"])
                    else:
                        print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes acima de -0.5 grau.")
                        myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                        return myJson
                else:
                    if currentSection["latitude"] < 0.5:
                        currentSection = findObjectByCoordinate((currentSection["latitude"] + 1), currentSection["longitude"])
                    else:
                        print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes acima de -0.5 grau.")
                        myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                        return myJson
        #caso necessite de uma nova seção em x (longitude)
        if auxDistance * myHorizontalProportion >= maxHorizontalDistance:
            if(resultingTrajectory[-1][1] <= -69.5):
                print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes abaixo de -69.5 graus.")
                myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                return myJson
            elif(resultingTrajectory[-1][1] >= -0.5):
                print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes acima de -0.5 grau.")
                myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                return myJson
            else:
                if(direction > 90 and direction < 270):
                    if currentSection["longitude"] > -69.5:
                        currentSection = findObjectByCoordinate(currentSection["latitude"], (currentSection["longitude"] - 1))
                    else:
                        print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes acima de -0.5 grau.")
                        myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                        return myJson
                else:
                    if currentSection["longitude"] < -0.5:    
                        currentSection = findObjectByCoordinate(currentSection["latitude"], (currentSection["longitude"] + 1))
                    else:
                        print("Essa trajetória necessita de dados não encontrados na base atual. Especificamente longitudes acima de -0.5 grau.")
                        myJson = prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
                        return myJson
        if currentSection["dataPerDate"][datePos]["direction"] == None:
            print("#{} Trajetória criada com êxito.".format(x))
            myJson =  prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
            return myJson
    print("#{} Trajetória criada com êxito.".format(x))
    myJson =  prepareReturnJSON(resultingTrajectory,  timeVector, distanceVector, x)
    return myJson
        

        



        
    
    
    
def prepareReturnJSON(resultingTrajectory, traveledTime, traveledDistance, y):
    return {
        "Trajetoria numero": y,
        "Coordenada inicial": resultingTrajectory[0],
        "Distancia percorrida (m)": traveledDistance,
        "Tempo viajado (s)": traveledTime,
        "Percurso viajado": resultingTrajectory
    }



def getInitialDate(initialDate, currentSection):
    initialDate = float(initialDate)
    for x in range(len(currentSection["dataPerDate"])):
        if(initialDate == float(currentSection["dataPerDate"][x]["date"])):
            return x
        elif(initialDate < float(currentSection["dataPerDate"][x]["date"])):
            return x-1

def getMaxDistances(direction, initialCoordenate):
    longDegreesPerHorizontalMeter = 0
    latDegreesPerVerticalMeter = 0
    
    #CASO DE x+ e y+
    if direction >= 0 and direction < 90: #tanto em x quanto em y aumenta
        #verificando longitude (x)
        if initialCoordenate[1] < 0: #exemplo: -27.5 -> -27 || -27 -> -26
            if initialCoordenate[1] == trunc(initialCoordenate[1]): #verificando se é int (sem casa decimal)
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], initialCoordenate[1] + 1])
                longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt((initialCoordenate[1]+1)**2))/maxHorizontalDistance
            else:
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1])])
                longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt(trunc(initialCoordenate[1])**2))/maxHorizontalDistance
        else: #exemplo: 28.5 -> 29 || 28 -> 29
            maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1] + 1)])
            longDegreesPerHorizontalMeter = (initialCoordenate[1] - (trunc(initialCoordenate[1])+1))/maxHorizontalDistance
        
        #verificando latitude (y)
        if initialCoordenate[0] < 0: #exemplo: -27.5 -> -27 || -27 -> -26
            if initialCoordenate[0] == trunc(initialCoordenate[0]):
                maxVerticalDistance = haversine(initialCoordenate, [initialCoordenate[0] + 1, initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((initialCoordenate[0] + 1)**2))/maxVerticalDistance
            else:
                maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0]), initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt(trunc(initialCoordenate[0])**2))/maxVerticalDistance
        else: #exemplo: 28.5 -> 29 || 28 -> 29
            maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0] + 1), initialCoordenate[1]])
            latDegreesPerVerticalMeter = (initialCoordenate[0] - trunc(initialCoordenate[0])+1)/maxVerticalDistance
    
    
    #CASO DE x- e y+
    elif(direction >= 90 and direction < 180):
        #verificando longitude (x)
        if(initialCoordenate[1] < 0): #x ex.: -27.5 => -28 || -27 => -28
            maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1]) - 1])
            longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt(trunc(initialCoordenate[1] - 1)**2))/maxHorizontalDistance
        else: #ex 27.3 => 27 || 27 => 26
            if initialCoordenate[1] == trunc(initialCoordenate[1]):
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], initialCoordenate[1] - 1])
                longDegreesPerHorizontalMeter = (initialCoordenate[1] - (initialCoordenate[1] - 1))/maxHorizontalDistance
            else:
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1])])
                longDegreesPerHorizontalMeter = (initialCoordenate[1] - trunc(initialCoordenate[1]))/maxHorizontalDistance

        #verificando latitude (y)
        if initialCoordenate[0] < 0: #exemplo: -27.5 -> -27 || -27 -> -26
            if initialCoordenate[0] == trunc(initialCoordenate[0]):
                maxVerticalDistance = haversine(initialCoordenate, [initialCoordenate[0] + 1, initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((initialCoordenate[0] + 1)**2))/maxVerticalDistance
            else:
                maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0]), initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt(trunc(initialCoordenate[0])**2))/maxVerticalDistance
        else: #exemplo: 28.5 -> 29 || 28 -> 29
            maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0] + 1), initialCoordenate[1]])
            latDegreesPerVerticalMeter = (initialCoordenate[0] - trunc(initialCoordenate[0])+1)/maxVerticalDistance
    
    
    #CASO DE x- e y-
    elif(direction >= 180 and direction < 270): # x - and y -
        #verificando longitude (x)
        if(initialCoordenate[1] < 0): #x ex.: -27.5 => -28 || -27 => -28
            maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1]) - 1])
            longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt(trunc(initialCoordenate[1] - 1)**2))/maxHorizontalDistance
        else: #ex 27.3 => 27 || 27 => 26
            if initialCoordenate[1] == trunc(initialCoordenate[1]):
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], initialCoordenate[1] - 1])
                longDegreesPerHorizontalMeter = (initialCoordenate[1] - (initialCoordenate[1] - 1))/maxHorizontalDistance
            else:
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1])])
                longDegreesPerHorizontalMeter = (initialCoordenate[1] - trunc(initialCoordenate[1]))/maxHorizontalDistance
        
        #verificando latitude (y)
        if(initialCoordenate[0] < 0): #ex.: -27.5 => -28   || -27 => -28
            maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0] - 1), initialCoordenate[1]])
            latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((trunc(initialCoordenate[0]) - 1)**2))/maxVerticalDistance
        else: #ex;: 27.5 => 27 || 27 => 26
            if initialCoordenate[0] == trunc(initialCoordenate[0]):
                maxVerticalDistance = haversine(initialCoordenate, [initialCoordenate[0] - 1, initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((initialCoordenate[0] - 1)**2))/maxVerticalDistance
            else: 
                maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0]), initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt(trunc(initialCoordenate[0])**2))/maxVerticalDistance
    
    
    #CASO X+ E Y-
    elif(direction >= 270 and direction <= 360): # x + and y -
        #verificando longitude (x)
        if initialCoordenate[1] < 0: #exemplo: -27.5 -> -27 || -27 -> -26
            if initialCoordenate[1] == trunc(initialCoordenate[1]): #verificando se é int (sem casa decimal)
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], initialCoordenate[1] + 1])
                longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt((initialCoordenate[1]+1)**2))/maxHorizontalDistance
            else:
                maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1])])
                longDegreesPerHorizontalMeter = (sqrt(initialCoordenate[1]**2) - sqrt(trunc(initialCoordenate[1])**2))/maxHorizontalDistance
        else: #exemplo: 28.5 -> 29 || 28 -> 29
            maxHorizontalDistance = haversine(initialCoordenate, [initialCoordenate[0], trunc(initialCoordenate[1] + 1)])
            longDegreesPerHorizontalMeter = (initialCoordenate[1] - (trunc(initialCoordenate[1])+1))/maxHorizontalDistance
        
        #verificando latitude (y)
        if(initialCoordenate[0] < 0): #ex.: -27.5 => -28   || -27 => -28
            maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0] - 1), initialCoordenate[1]])
            latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((trunc(initialCoordenate[0]) - 1)**2))/maxVerticalDistance
        else: #ex;: 27.5 => 27 || 27 => 26
            if initialCoordenate[0] == trunc(initialCoordenate[0]):
                maxVerticalDistance = haversine(initialCoordenate, [initialCoordenate[0] - 1, initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt((initialCoordenate[0] - 1)**2))/maxVerticalDistance
            else: 
                maxVerticalDistance = haversine(initialCoordenate, [trunc(initialCoordenate[0]), initialCoordenate[1]])
                latDegreesPerVerticalMeter = (sqrt(initialCoordenate[0]**2) - sqrt(trunc(initialCoordenate[0])**2))/maxVerticalDistance  
        
    if longDegreesPerHorizontalMeter < 0:
        longDegreesPerHorizontalMeter = sqrt(longDegreesPerHorizontalMeter**2)  
    if latDegreesPerVerticalMeter < 0: 
        latDegreesPerVerticalMeter = sqrt(latDegreesPerVerticalMeter**2)
    return {"maxHorizontalDistance": maxHorizontalDistance, "maxVerticalDistance": maxVerticalDistance, "longDegreesPerHorizontalMeter": longDegreesPerHorizontalMeter, "latDegreesPerVerticalMeter": latDegreesPerVerticalMeter,}
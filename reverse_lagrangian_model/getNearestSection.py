from haversine import haversine


def getNearestSection(coord1: object, allData):
    distance = 10000000000000
    for x in range(len(allData)):
        if allData[x]['dataPerDate'][0]['resultingVelocity'] != None and allData[x]['coast'] != True:
            auxCoord = [allData[x]['latitude'], allData[x]['longitude']]
            if(coord1 != auxCoord):
                if haversine(coord1, auxCoord) < distance:
                    distance =  haversine(coord1, auxCoord)
                    coord2 = auxCoord
    return [coord2, distance]
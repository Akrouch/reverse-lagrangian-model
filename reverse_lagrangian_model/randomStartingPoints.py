import random
from math import trunc


def randomStartingPoints(nearestSection, total): 
    initials = []
    for x in range(total):
        if(nearestSection['latitude'] > 0 and nearestSection['longitude'] > 0): #28
            initials.append([round(trunc(nearestSection['latitude']) + random.uniform(0.3, 0.7), 2), trunc(nearestSection['longitude'])])
        elif nearestSection['latitude'] > 0: #-28
            initials.append([round(trunc(nearestSection['latitude']) + random.uniform(0.3, 0.7), 2), (trunc(nearestSection['longitude']) - 1)])
        elif nearestSection['longitude'] > 0:
            initials.append([round(trunc(nearestSection['latitude']) - random.uniform(0.3, 0.7), 2), trunc(nearestSection['longitude'])])
        else:
            initials.append([round(trunc(nearestSection['latitude']) - random.uniform(0.3, 0.7), 2), (trunc(nearestSection['longitude']) - 1)])
    return initials
from pymongo import MongoClient
import csv


client = MongoClient('mongodb+srv://scientist:reverse123@cluster0.9beat.mongodb.net/test')


db = client.reverseLagrangianModel
col = db.sections_with_coast_treatment
allProcessedData = []
for doc in col.find():
    allProcessedData.append(doc)


with open('saidas/original_resultante.csv', mode='w') as csv_file:
    fieldnames = ['id', 'Latitude', 'Longitude', 'Data', 'Vel.Resultante', 'Direção.Resultante']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    for x in range(len(allProcessedData)):
        print(x)
        for t in range(len(allProcessedData[x]['dataPerDate'])):
            if allProcessedData[x]['dataPerDate'][t]['resultingVelocity'] == None:
                vel = "Null"
                direction = "Null"
            else:
                vel = allProcessedData[x]['dataPerDate'][t]['resultingVelocity']
                direction = allProcessedData[x]['dataPerDate'][t]['direction']
            writer.writerow({'id': x, 'Latitude': allProcessedData[x]['latitude'], 'Longitude': allProcessedData[x]['longitude'], 'Data': allProcessedData[x]['dataPerDate'][t]['date'], 'Vel.Resultante':vel, 'Direção.Resultante': direction})
    print("CSV da base gerada com êxito.")
    
    
# with open('saidas/original_componentes.csv', mode='w') as csv_file:
#     fieldnames = ['Latitude', 'Longitude', 'Data', 'Uf', 'Vf']
#     writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
#     writer.writeheader()
#     lati = 0.5
#     for y in range(0, len(allData['y'])):
#         longi = -69.5
#         for x in range(0, len(allData['x'])):
#             for t in range(0, len(allData['dates'])):
#                 tmpX = allData['uf'][t][y][x] #X (longitudinal) cateto adjacente
#                 tmpY = allData['vf'][t][y][x]
#                 writer.writerow({'Latitude': lati, 'Longitude': longi, 'Data': allData['dates'][t], 'Uf': tmpX, 'Vf': tmpY})
#             longi += 1
#         lati -= 1
#     print("CSV da base gerada com êxito.")
    
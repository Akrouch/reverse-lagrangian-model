from pprint import pprint
from allData import allData
import json





def add_extra_sections(db):
    lati = 0.5
    for y in range(0, len(allData['y'])):
        foundCoast = False
        longi = -0.5
        for x in range(0, len(allData['x'])):
            result = db.sections_with_coast_treatment.find_one({"latitude": lati, "longitude": longi})
            if result['dataPerDate'][0]['direction'] == None and foundCoast == False:
                foundCoast = True
                myAux = db.sections_with_coast_treatment.find_one({"latitude": lati, "longitude": longi+1})
                dataPerDate = myAux['dataPerDate']
                print('Costa => Lat: {} Long: {}'.format(lati, longi))
                update = db.sections_with_coast_treatment.update_one({"latitude": lati, "longitude": longi}, 
                    {
                        "$set":
                        {
                            "coast": True,
                            "dataPerDate": dataPerDate
                        }
                    }
                )
                result = db.sections_with_coast_treatment.find_one({"latitude": lati, "longitude": longi})
                print("Alterou: {}".format(result))
                print("Alteração: {}".format(update))
            longi -= 1
        print('------------------------------------------------------ y = {} over -----------------------------------------------'.format(y))
        lati -= 1





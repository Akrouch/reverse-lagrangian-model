from allData import allData
from math import degrees
from math import atan
from math import sqrt


#function works for a dataset of the previous month (7 updates of velocity)
def getResultingVelAndDir(x, y):
    result = [
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            },
            {
                "velocity": None,
                "direction": None
            }
        ]
    for t in range(0, len(allData['dates'])):
        tmpX = allData['uf'][t][y][x] #X (longitudinal) cateto adjacente
        tmpY = allData['vf'][t][y][x] #Y (latitudinal) cateto oposto
        if isinstance(tmpX, str) and isinstance(tmpY, str):
            result[t]['velocity'] = None
            result[t]['direction'] = None
        elif isinstance(tmpX, str):
            result[t]['velocity'] = round(sqrt(tmpY*tmpY), 3)
            if tmpY < 0:
                result[t]['direction']: 90
            else:
                result[t]['direction']: 270
        elif isinstance(tmpY, str):
            result[t]['velocity'] = round(sqrt(tmpX*tmpX), 3)
            if tmpX < 0:
                result[t]['direction']: 0
            else:
                result[t]['direction']: 180
        else:
            # #ORIGINAL
            # result[t]['direction'] = round(degrees(atan(tmpY/tmpX)), 2)
            # if tmpX > 0 and tmpY < 0:
            #     result[t]['direction'] += 360
            # if tmpX < 0:
            #     result[t]['direction'] += 180
            #INVERSA
            result[t]['direction'] = round(degrees(atan(tmpY/tmpX)), 2)
            if tmpX > 0:
                result[t]['direction'] += 180
            elif tmpX < 0 and tmpY > 0:
                result[t]['direction'] += 360
            result[t]['velocity'] = round(sqrt((tmpX*tmpX) + (tmpY * tmpY)), 3)
           
    return result
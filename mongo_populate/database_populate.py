from pymongo import MongoClient
from pprint import pprint
from allData import allData
from add_extra_sections import add_extra_sections
from resultingVelAndDirection import getResultingVelAndDir

client = MongoClient('mongodb+srv://scientist:reverse123@cluster0.9beat.mongodb.net/test')

db = client.reverseLagrangianModel



lati = 0.5
for y in range(0, len(allData['y'])):
    longi = -69.5
    for x in range(0, len(allData['x'])):
        tmp = getResultingVelAndDir(x,y)
        result = db.sections_with_coast_treatment.insert_one({ 
            "latitude": lati, 
            "longitude": longi, 
            "coast": False,
            "dataPerDate": [
                {
                    "date": allData['dates'][0],
                    "resultingVelocity": tmp[0]['velocity'],
                    "direction": tmp[0]['direction']
                },
                {
                    "date": allData['dates'][1],
                    "resultingVelocity": tmp[1]['velocity'],
                    "direction": tmp[1]['direction']
                },
                {
                    "date": allData['dates'][2],
                    "resultingVelocity": tmp[2]['velocity'],
                    "direction": tmp[2]['direction']
                },
                {
                    "date": allData['dates'][3],
                    "resultingVelocity": tmp[3]['velocity'],
                    "direction": tmp[3]['direction']
                },
                {
                    "date": allData['dates'][4],
                    "resultingVelocity": tmp[4]['velocity'],
                    "direction": tmp[4]['direction']
                },
                {
                    "date": allData['dates'][5],
                    "resultingVelocity": tmp[5]['velocity'],
                    "direction": tmp[5]['direction']
                },
                {
                    "date": allData['dates'][6],
                    "resultingVelocity": tmp[6]['velocity'],
                    "direction": tmp[6]['direction']
                }]
        }
        )
        longi += 1
    print('------------------------------------------------------ y = {} over -----------------------------------------------'.format(y))
    lati -= 1
    
add_extra_sections(db)



